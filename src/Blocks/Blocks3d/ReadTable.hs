{-# LANGUAGE DeriveAnyClass        #-}
{-# LANGUAGE DerivingStrategies    #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE OverloadedRecordDot   #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE TypeOperators         #-}

module Blocks.Blocks3d.ReadTable
  ( BlockTable (..)
  , DerivMatrixKey (..)
  , DerivMatrix (..)
  , DerivMap
  , readBlockFile
  , readBlockTable
  , readBlockFileAeson
  , readBlockTableAeson
  , QuotedFractional (..)
  ) where

import Blocks.Blocks3d.BlockTableKey        (BlockTableKey)
import Blocks.Blocks3d.Parity               (Parity)
import Blocks.Blocks3d.WriteTable           (blockTableFilePath)
import Blocks.Coordinate                    (Coordinate)
import Blocks.ScalarBlocks.Types            (FourRhoCrossing)
import Bootstrap.Math.DampedRational        qualified as DR
import Bootstrap.Math.HalfInteger           (HalfInteger)
import Bootstrap.Math.Polynomial            (Polynomial (..), mapCoefficients)
import Bootstrap.Math.Polynomial            qualified as Polynomial
import Control.Applicative                  (Alternative (..), ZipList (..),
                                             many, (<|>))
import Control.DeepSeq                      (NFData)
import Control.Exception                    (Exception, throw)
import Control.Monad                        (MonadPlus, liftM2, mzero, void)
import Control.Monad.Trans.Resource         (runResourceT)
import Data.Aeson                           (FromJSON (..))
import Data.Aeson                           qualified as Aeson
import Data.Aeson.Parser                    qualified as AesonParser
import Data.Attoparsec.ByteString.Char8     qualified as Atto
import Data.Attoparsec.ByteString.Streaming qualified as StreamingAtto
import Data.ByteString                      (ByteString)
import Data.Functor.Compose                 (Compose (..))
import Data.Map.Strict                      (Map)
import Data.Map.Strict                      qualified as Map
import Data.Matrix                          (Matrix)
import Data.Matrix                          qualified as Matrix
import Data.MultiSet                        qualified as MultiSet
import Data.Proxy                           (Proxy (..))
import Data.Reflection                      (Reifies, reflect)
import Data.Text                            qualified as Text
import Data.Text.Read                       qualified as Text
import Data.Vector                          (Vector)
import GHC.Generics                         (Generic)
import Hyperion.Log                         qualified as Log
import Streaming                            qualified as Streaming
import Streaming.ByteString                 (ByteStream)
import Streaming.ByteString                 qualified as ByteStream
import Streaming.Prelude                    qualified as Streaming

type DerivMap = Map (Int, Int)

-- | These record field names match the schema used by blocks_3d
data IndexValues = IndexValues
  { three_pt_parity_120 :: Parity
  , three_pt_parity_430 :: Parity
  -- | j_120 and j_430 are Vectors because they will be used
  -- unmodified below
  , j_120               :: Vector HalfInteger
  , j_430               :: Vector HalfInteger
  -- | coordinate is a list because this is more convenient for
  -- looping in FromJSON (BlockTable a)
  , coordinates         :: [Coordinate]
  } deriving (Generic, FromJSON)

-- | For internal use with DuplicateRecordFields
coordinateIV :: IndexValues -> [Coordinate]
coordinateIV iv = iv.coordinates

textToFractional :: (MonadPlus m, Fractional a) => Text.Text -> m a
textToFractional t = case Text.rational t of
  Right (x, "") -> return x
  _             -> mzero

newtype QuotedFractional a = QuotedFractional { unQuoted :: a }
  deriving newtype (Eq, Num)

instance Fractional a => FromJSON (QuotedFractional a) where
  parseJSON = Aeson.withText "QuotedFractional" (fmap QuotedFractional . textToFractional)

newtype PolynomialJSON a = PolynomialJSON { toPolynomial :: Polynomial a }

instance (Fractional a, Eq a) => FromJSON (PolynomialJSON a) where
  parseJSON = fmap (PolynomialJSON . mapCoefficients unQuoted) . parseJSON

newtype DerivMapJSON a = DerivMapJSON { toDerivMap :: DerivMap (Polynomial a) }

unNestVectors2 :: [[a]] -> Map.Map (Int,Int) a
unNestVectors2 vecs = Map.fromList $ do
  (i, vecs_i)  <- zip [0..] vecs
  (j, vecs_ij) <- zip [0..] vecs_i
  pure ((i,j), vecs_ij)

instance (Fractional a, Eq a) => FromJSON (DerivMapJSON a) where
  parseJSON =
    fmap (DerivMapJSON . unNestVectors2 . fmap (fmap toPolynomial)) . parseJSON

-- | These record field names match the schema used by blocks_3d
data DerivDataJSON a = DerivDataJSON
  { index_values  :: [IndexValues]
  , derivs        :: [[[[DerivMapJSON a]]]]
  , pole_list_x   :: [Rational]
  , delta_minus_x :: Rational
  } deriving (Generic, FromJSON)

-- | For internal use with DuplicateRecordFields
derivsDDJ :: DerivDataJSON a -> [[[[DerivMapJSON a]]]]
derivsDDJ ddj = ddj.derivs

type Block3dDR a = DR.DampedRational (FourRhoCrossing a) (Matrix `Compose` DerivMap) a

data DerivMatrix a = DerivMatrix
  { j120s  :: Vector HalfInteger
  , j430s  :: Vector HalfInteger
  , derivs :: Block3dDR a
  } deriving (Generic, NFData)

data DerivMatrixKey = DerivMatrixKey
  { threePtParity120 :: Parity
  , threePtParity430 :: Parity
  , coordinates      :: Coordinate
  } deriving (Eq, Ord, Show, Generic, NFData)

data BlockTable a = BlockTable
  { derivMatrices :: Map DerivMatrixKey (DerivMatrix a)
  , deltaMinusX   :: Rational
  } deriving (Generic, NFData)

transposeLists :: [[a]] -> [[a]]
transposeLists = getZipList . traverse ZipList

-- | Given a shift y, numerator N(x) and a list of poles p_i, forms
-- the DampedRational
--
-- b^(x+y) N(x) / prod_i (x - p_i)
--
-- where b is the base of the DampedRational.
dampedRationalWithShift
  :: forall base f a . (Floating a, Eq a, Reifies base a, Functor f)
  => Rational
  -> f (Polynomial a)
  -> [Rational]
  -> DR.DampedRational base f a
dampedRationalWithShift y numerator poles =
  DR.scaleDampedRational expFactor $
  DR.DampedRational numerator (MultiSet.fromList poles)
  where
    b = reflect @base Proxy
    expFactor = exp (fromRational y * log b)

derivDataJsonToBlockTable :: (Floating a, Eq a) => DerivDataJSON a -> BlockTable a
derivDataJsonToBlockTable derivData =
  let dMx = delta_minus_x derivData
  in BlockTable
     { derivMatrices = Map.fromList $ do
         -- derivs derivData is a four-fold nested list of
         -- DerivMapJSON's. The index levels are:
         --  (1) three_pt_parity_index
         --  (2) j_120_index
         --  (3) j_430_index
         --  (4) coordinate_index
         -- The first step is to loop over the three_pt_parity_index
         (indexVals, indexDeriv)  <-
           zip (index_values derivData) (derivsDDJ derivData)
         -- Next, we must bring coordinate_index to the top level, so
         -- we transpose indexDeriv twice and loop over the
         -- coordinates
         (coord, coordIndexDeriv) <- zip (coordinateIV indexVals)
                                     (transposeLists (fmap transposeLists indexDeriv))
         -- Finally, we can form the DerivMatrix from coordIndexDeriv
         pure ( DerivMatrixKey
                { threePtParity120 = three_pt_parity_120 indexVals
                , threePtParity430 = three_pt_parity_430 indexVals
                , coordinates = coord
                }
              , DerivMatrix
                { j120s  = j_120 indexVals
                , j430s  = j_430 indexVals
                -- The polynomials in the block tables must be
                -- multipled by (4 rho)^Delta = (4 rho)^dMx*(4
                -- rho)^x. The function 'dampedRationalWithShift'
                -- takes care of multiplying by the extra (4
                -- rho)^dMx factor.
                , derivs = dampedRationalWithShift dMx
                           (Compose $ toDerivMap <$> Matrix.fromLists coordIndexDeriv)
                           (pole_list_x derivData)
                }
              )
     , deltaMinusX = dMx
     }

instance (Floating a, Eq a) => FromJSON (BlockTable a) where
  parseJSON = fmap derivDataJsonToBlockTable . parseJSON

data ReadBlockTableException = ReadBlockTableException
  { exceptionKey  :: BlockTableKey
  , exceptionFile :: FilePath
  , exceptionMsg  :: String
  } deriving (Show, Exception)

-- | Read a block file using Aeson.FromJSON. This reads the whole file
-- into memory and parses it as a json tree, before turning it into a
-- BlockTable, so it can take much more memory than the streaming
-- versions below. However, the streaming versions are more delicate
-- in that they cannot support json comments, reordering of keys,
-- etc. We continue to provide these versions for debugging and
-- testing purposes.
readBlockFileAeson
  :: (Floating a, Eq a)
  => FilePath
  -> IO (Either String (BlockTable a))
readBlockFileAeson file = do
  Log.info "Reading Block3d table" file
  Aeson.eitherDecodeFileStrict' file

readBlockTableAeson
  :: (Floating a, Eq a)
  => FilePath
  -> BlockTableKey
  -> IO (BlockTable a)
readBlockTableAeson blockTableDir key =
  either (throw . ReadBlockTableException key file) id <$>
  readBlockFileAeson file
  where
    file = blockTableFilePath blockTableDir key

------------ Streaming parsing -------------

-- | Aeson's default parser builds up an entire tree of json data in
-- memory before parsing it down to size. We would like to avoid
-- this. We would also like to strictly parse ByteString's
-- representing floating point numbers into BigFloat's, without
-- retaining the ByteString's in memory. To achieve this, we make 2 major changes:
--
-- 1) We parse from a ByteStream, via
--    'ByteStreamParser'. This allows us to mix and match raw Attoparsec
--    parsers and Aeson parsers.
--
-- 2) Note that 'derivs' is a 7-fold nested array of BigFloat's. We
--    first parse it into a strict stream of 'ArrayPiece's, Note that
--    the 'ArrayPiece's are strict and do not allow ByteString's to
--    hang around in thunks. We then use a separate pure 'TokenParser'
--    to construct the nested arrays from a list of 'ArrayPiece's.

-- | Parser for extracting data from a ByteStream. This is
-- essentially 'StateT (ByteStream m r) m a'. Note that we
-- do not allow for error handling via 'Either String'. Errors will
-- result in thrown exceptions. This is because we would like to parse
-- block files without backtracking, and any error should be fatal.
newtype ByteStreamParser m r a =
  MkByteStreamParser
  { runByteStreamParser :: ByteStream m r -> m (a, ByteStream m r) }
  deriving (Functor)

instance Monad m => Applicative (ByteStreamParser m r) where
  pure x = MkByteStreamParser $ \stream -> pure (x, stream)
  (<*>) = liftM2 ($)

instance Monad m => Monad (ByteStreamParser m r) where
  MkByteStreamParser go1 >>= k = MkByteStreamParser $ \stream -> do
    (x, stream') <- go1 stream
    let MkByteStreamParser go2 = k x
    go2 stream'

-- | "Lift" an Attoparsec.Parser to a ByteStreamParser
liftAttoparsec :: Monad m => Atto.Parser a -> ByteStreamParser m r a
liftAttoparsec parser = MkByteStreamParser $ \stream -> do
  res <- StreamingAtto.parse parser stream
  pure $ case res of
    (Left errs, _)     -> error (show errs)
    (Right x, stream') -> (x, stream')

-- | A 'ByteStreamParser' for anything with a FromJSON instance
aesonValue :: (FromJSON a, Monad m) => ByteStreamParser m r a
aesonValue = do
  val <- liftAttoparsec AesonParser.value
  case Aeson.fromJSON val of
    Aeson.Error err -> error err
    Aeson.Success x -> pure x

-- | Ignore opening and closing quotes around the given parser
quoted :: Atto.Parser a -> Atto.Parser a
quoted p = Atto.char '"' *> p <* Atto.char '"'

-- | Ignore spaces around the given parser
spaced :: Atto.Parser a -> Atto.Parser a
spaced p = Atto.skipSpace *> p <* Atto.skipSpace

-- | Skip a given character
skipChar :: Monad m => Char -> ByteStreamParser m r ()
skipChar c = liftAttoparsec (void (spaced (Atto.char c)))

-- | Match a quoted string followed by a colon:
--
-- "my_key":
--
-- Note that we require the key name to be quoted -- otherwise
-- the parser will fail.
jsonKey :: Monad m => ByteString -> ByteStreamParser m r ()
jsonKey key = liftAttoparsec (quoted (Atto.string key) *> void (spaced (Atto.char ':')))

-- | Match a key-value pair, where the value is an instance of
-- 'FromJSON'
jsonKeyVal' :: forall a m r . (Monad m, FromJSON a) => ByteString -> ByteStreamParser m r a
jsonKeyVal' key = jsonKey key *> aesonValue

-- | Match a key-value pair, and skip a trailing comma
jsonKeyVal :: forall a m r . (Monad m, FromJSON a) => ByteString -> ByteStreamParser m r a
jsonKeyVal key = jsonKeyVal' key <* skipChar ','

-- | Skip a key, and the comma after it
skipKey :: Monad m => ByteString -> ByteStreamParser m r ()
skipKey key = void (jsonKeyVal @Aeson.Value key)

---------- Parsing derivs ----------

-- | We parse derivs by first parsing the ByteStream into a
-- '[ArrayPiece a]'. What is the best way to turn a list of
-- 'ArrayPiece's into nested arrays of 'a's?  Actually, it's by
-- parsing!  This time the parser acts over a list of 'ArrayPiece's
-- instead of a String or ByteString. We implement our own TokenParser
-- for this purpose. Note that we could re-use the parser types in a
-- library like parsec or megaparsec, but they are overpowered for our
-- purposes.

-- | Tokens that appear when parsing nested arrays: an OpenBrace '[',
-- a CloseBrace ']', and an array element. Commas are skipped.
data ArrayPiece a = Element !a | OpenBrace | CloseBrace
  deriving (Eq, Ord, Show, Generic, NFData)

-- | Parse an ArrayPiece.
arrayPiece :: Fractional a => Atto.Parser (ArrayPiece a)
arrayPiece =
  (Atto.char '[' *> pure OpenBrace)
  <|> (Atto.char ']' *> pure CloseBrace)
  <|> (Element <$> quoted Atto.rational)

type Error = String

-- | A parser for a list of tokens of type 't'.
newtype TokenParser t a = MkTokenParser { runTokenParser :: [t] -> (Either Error a, [t]) }
  deriving (Functor)

instance Applicative (TokenParser t) where
  pure x = MkTokenParser $ \stream -> (Right x, stream)
  MkTokenParser go1 <*> MkTokenParser go2 = MkTokenParser $ \stream ->
    case go1 stream of
      (Left err, stream') -> (Left err, stream')
      (Right f, stream') -> case go2 stream' of
        (Left err, stream'') -> (Left err, stream'')
        (Right x, stream'')  -> (Right (f x), stream'')

instance Alternative (TokenParser t) where
  empty = MkTokenParser $ \stream -> (Left "empty", stream)
  MkTokenParser go1 <|> MkTokenParser go2 = MkTokenParser $ \stream ->
    case go1 stream of
      (Left _, _) -> go2 stream
      res         -> res

-- | Parse an OpenBrace
openBrace :: TokenParser (ArrayPiece a) ()
openBrace = MkTokenParser $ \stream -> case stream of
  OpenBrace : stream' -> (Right (), stream')
  _                   -> (Left "openBrace", stream)

-- | Parse a CloseBrace
closeBrace :: TokenParser (ArrayPiece a) ()
closeBrace = MkTokenParser $ \stream -> case stream of
  CloseBrace : stream' -> (Right (), stream')
  _                    -> (Left "closeBrace", stream)

-- | Parse an array element
element :: TokenParser (ArrayPiece a) a
element = MkTokenParser $ \stream -> case stream of
  Element x : stream' -> (Right x, stream')
  _                   -> (Left "element", stream)

-- | Given a parser for 'b', match a list [b] by consuming an
-- OpenBrace, a bunch of elements, and a CloseBrace.
listOf :: TokenParser (ArrayPiece a) b -> TokenParser (ArrayPiece a) [b]
listOf elt = openBrace *> many elt <* closeBrace

-- | Parse a Polynomial
polynomial :: (Num a, Eq a) => TokenParser (ArrayPiece a) (Polynomial a)
polynomial = Polynomial.fromList <$> listOf element

-- | Parse a DerivMap of Polynomial's
derivMap :: (Num a, Eq a) => TokenParser (ArrayPiece a) (DerivMap (Polynomial a))
derivMap = unNestVectors2 <$> listOf (listOf polynomial)

-- | Parse derivs as a 4-fold nested list of 'DerivMap (Polynomial a)'s
derivsTokenParser :: (Num a, Eq a) => TokenParser (ArrayPiece a) [[[[DerivMap (Polynomial a)]]]]
derivsTokenParser = listOf (listOf (listOf (listOf derivMap)))

-- | Parse a list of ArrayPiece's from a ByteStream
parseFractionalArray :: (Fractional a, Monad m) => ByteStreamParser m r [ArrayPiece a]
parseFractionalArray = MkByteStreamParser $ \stream -> do
  pieces Streaming.:> rest <- Streaming.toList $
    StreamingAtto.parsed
    (arrayPiece <* Atto.skipSpace <* Atto.skipWhile (== ',') <* Atto.skipSpace)
    stream
  case rest of
    Left (_errors, stream') -> pure (pieces, stream')
    Right result            -> pure (pieces, pure result)
  
-- | Parse derivs from a ByteStream by running 'parseFractionalArray'
-- and then running our 'TokenParser' on the list of 'ArrayPart's.
parseDerivs :: (Fractional a, Eq a, Monad m) => ByteStreamParser m r [[[[DerivMap (Polynomial a)]]]]
parseDerivs = MkByteStreamParser $ \stream -> do
  (arrayPieces, stream') <- runByteStreamParser parseFractionalArray stream
  pure $
    case runTokenParser derivsTokenParser arrayPieces of
      (Right derivs, []) -> (derivs, stream')
      (Right _, _)       -> error "leftover tokens"
      (Left err, _)      -> error err

-- | Parse a BlockTable from a ByteStream. Note that this
-- implementation is much less forgiving than we usually get using
-- Aeson. We must specify precisely which keys we encounter, and in
-- what order, including keys that we skip. If the output of blocks_3d
-- is ever modified, this Parser will probably need to be modified
-- too.
blockTable :: (Eq a, Floating a, Monad m) => ByteStreamParser m r (BlockTable a)
blockTable = do
  skipChar '{'
  mapM_ skipKey
    [ "j_external"
    , "j_internal"
    ]
  delta_minus_x <- jsonKeyVal "delta_minus_x"
  mapM_ skipKey
    [ "j_12"
    , "j_43"
    , "delta_12"
    , "delta_43"
    , "delta_1_plus_2"
    , "four_pt_struct"
    , "four_pt_sign"
    , "order"
    , "lambda"
    , "kept_pole_order"
    , "coordinates"
    ]
  pole_list_x <- jsonKeyVal "pole_list_x"
  mapM_ skipKey
    [ "precision_input"
    , "precision_actual"
    , "j_internal_input"
    , "output_template"
    , "args"
    ]
  index_values <- jsonKeyVal "index_values"
  mapM_ skipKey ["index_names"]
  derivs <- jsonKey "derivs" *> parseDerivs
  skipChar '}'
  pure $
    derivDataJsonToBlockTable $
    DerivDataJSON
    { index_values  = index_values
    , derivs        = fmap (fmap (fmap (fmap DerivMapJSON))) derivs
    , pole_list_x   = pole_list_x
    , delta_minus_x = delta_minus_x
    }

-- | Read a BlockTable from a file by constructing a ByteStream and
-- running the 'blockTable' ByteStreamParser.
readBlockFileStreaming
  :: (Floating a, Eq a)
  => FilePath
  -> IO (BlockTable a)
readBlockFileStreaming file = do
  Log.info "Reading Block3d table (streaming)" file
  (block, _rest) <- runResourceT $
    runByteStreamParser blockTable $
    ByteStream.readFile file
  pure block

readBlockTableStreaming
  :: (Floating a, Eq a)
  => FilePath
  -> BlockTableKey
  -> IO (BlockTable a)
readBlockTableStreaming blockTableDir key = readBlockFileStreaming $
  blockTableFilePath blockTableDir key

readBlockFile
  :: (Floating a, Eq a)
  => FilePath
  -> IO (BlockTable a)
readBlockFile = readBlockFileStreaming

readBlockTable
  :: (Floating a, Eq a)
  => FilePath
  -> BlockTableKey
  -> IO (BlockTable a)
readBlockTable = readBlockTableStreaming

