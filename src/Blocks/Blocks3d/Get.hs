{-# LANGUAGE ApplicativeDo       #-}
{-# LANGUAGE DataKinds           #-}
{-# LANGUAGE DeriveAnyClass      #-}
{-# LANGUAGE DerivingStrategies  #-}
{-# LANGUAGE MultiWayIf          #-}
{-# LANGUAGE NamedFieldPuns      #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE StaticPointers      #-}
{-# LANGUAGE TypeFamilies        #-}

module Blocks.Blocks3d.Get where

import Blocks                        (Block (..), BlockBase, BlockFetchContext,
                                      BlockTableParams, ContinuumBlock (..),
                                      Coordinate (..), CoordinateDir (..),
                                      CrossingMat, Delta (..), Derivative (..),
                                      HasBlocks, IsolatedBlock (..),
                                      KnownCoordinate (..), Sign, zzbDerivsAll)
import Blocks.Blocks3d.BlockTableKey (BlockTableKey)
import Blocks.Blocks3d.BlockTableKey qualified as Key
import Blocks.Blocks3d.Parity        (Parity, parityFromIntegral)
import Blocks.Blocks3d.ReadTable     (BlockTable (..), DerivMap,
                                      DerivMatrix (..), DerivMatrixKey (..))
import Blocks.ScalarBlocks.Types     (FourRhoCrossing)
import Blocks.Sign                   qualified as Sign
import Bootstrap.Bounds              (ThreePointStructure (..), ToTeX (..))
import Bootstrap.Bounds              qualified as Bounds
import Bootstrap.Build               (Fetches, fetch)
import Bootstrap.Math.DampedRational qualified as DR
import Bootstrap.Math.HalfInteger    (HalfInteger (..), fromHalfInteger)
import Bootstrap.Math.HalfInteger    qualified as HalfInteger
import Bootstrap.Math.Util           (choose, pochhammer)
import Control.DeepSeq               (NFData)
import Data.Aeson                    (FromJSON, ToJSON)
import Data.Binary                   (Binary)
import Data.Foldable                 (toList)
import Data.Foldable                 qualified as Foldable
import Data.Functor.Compose          (Compose (..))
import Data.Map.Strict               qualified as Map
import Data.Matrix                   qualified as Matrix
import Data.Maybe                    (fromMaybe)
import Data.Proxy                    (Proxy (..))
import Data.Reflection               (reflect)
import Data.Rendered                 (mkRendered)
import Data.Set                      qualified as Set
import Data.Tagged                   (Tagged)
import Data.Text                     qualified as Text
import Data.Vector                   qualified as V
import GHC.Generics                  (Generic)
import GHC.TypeLits
import Hyperion                      (Dict (..), Static (..))
import Numeric.Rounded               (Rounded, RoundingMode (TowardZero))

{- |

[Note: Conventions for conformal blocks.] For conventions in
1907.11247 (see app. A.5), the following reality properties hold:

1. OPE coeff are real if all 3 ops are bosonic and pure imaginary if
   any 2 are fermionic

2. The four-pt functions are real if there are 0 or 4 fermions. They
   are imaginary if there are 2 fermions.

3. This implies that the conformal blocks are real for bosonic
   exchanges and imaginary for fermionic exchanges.

We change this convention as follows.

1'. OPE coeffs are always real. This is achieved by lambda_there = i
   lambda_here, when there are 2 fermionic operators. Otherwise
   lambda_there = lambda_here.

2'. Four-pt functions are always real. This is achieved by 4pt_there =
   i 4pt_here when there are 2 fermionic operators in the 4pt, and
   4pt_there = 4pt_here otherwise.

These relations are nice because they are permutation-invariant. This
implies that conformal blocks are always real.

The relation between conformal blocks there and conformal blocks here
is thus as follows.

If there are 0 external fermions, the blocks are equal. If there are 4
external fermions, the blocks differ by a sign. If there are 2
external ferimions, then

Fermionic exchange:
  block_here = (-1 from 2 3pts) (i^(-1) from 4pt) block_there
Bosonic exchange
  block_here = (i from 1 3pts) (i^(-1) from 4pt) block_there

blocks_3d computes block_there for bosonic exchange and i^(-1)
block_there for fermionic exchange. This means

Fermionic exchange:
  block_here = (-1 from 2 3pts) (i^(-1) from 4pt) (i from block_3d def) blocks_3d = - blocks_3d
Bosonic exchange
  block_here = (i from 1 3pts) (i^(-1) from 4pt) blocks_3d = blocks_3d

So it seems we just need to multiply blocks_3d result by -1 for
fermionic exchanges or when there are 4 fermions. This is taken
care of in getShiftAndBlockVector for blocks loaded from blocks_3d.

Identity block generator follows these conventions.

-}

type BigFloat p = Rounded 'TowardZero p

showRationalWithPrecision :: Int -> Rational -> String
showRationalWithPrecision precision r =
  case someNatVal (fromIntegral precision) of
    Just (SomeNat (_ :: Proxy p)) -> show (fromRational @(BigFloat p) r)
    Nothing                       -> error "Negative precision not allowed"

data SO3Struct a1 a2 a3 = SO3Struct
  { operator1  :: ConformalRep a1
  , operator2  :: ConformalRep a2
  , operator3  :: ConformalRep a3
  , structj12  :: HalfInteger
  , structj123 :: HalfInteger
  }
  deriving (Eq, Ord, Show, Generic, Binary, FromJSON, ToJSON, NFData)

instance ToTeX (SO3Struct Rational Rational Delta) where
  toTeX (SO3Struct r1 r2 r3 j12 j123) =
    "["<> toTeX r1 <>"," <> toTeX r2 <> "," <> toTeX r3 <>"|" <> toTeX (SO3StructLabel j12 j123) <> "]"

-- TODO: just put this inside SO3Struct
data SO3StructLabel = SO3StructLabel
  { structj12'  :: HalfInteger
  , structj123' :: HalfInteger
  } deriving (Eq, Ord, Show, Generic, Binary, FromJSON, ToJSON, NFData)

instance ToTeX SO3StructLabel where
  toTeX (SO3StructLabel j12 j123) = "|" <> toTeX j12 <> "," <> toTeX j123 <> "\\rangle"

instance Static (Binary SO3StructLabel) where closureDict = static Dict
instance Static (Show SO3StructLabel)   where closureDict = static Dict

instance ThreePointStructure (SO3Struct a a a') SO3StructLabel (ConformalRep a) (ConformalRep a') where
  makeStructure o1 o2 o3 (SO3StructLabel j12 j123) =
    SO3Struct o1 o2 o3 j12 j123

data Q4Struct = Q4Struct
  { q4qs   :: (HalfInteger, HalfInteger, HalfInteger, HalfInteger)
  , q4Sign :: Sign 'YDir
  } deriving (Eq, Ord, Show, Generic, Binary, FromJSON, ToJSON, NFData)

instance ToTeX Q4Struct where
  toTeX (Q4Struct (q1,q2,q3,q4) ySign) =
    "\\<" <> Text.intercalate "," (map toTeX [q1,q2,q3,q4]) <> "\\>^" <> toTeX ySign

data ConformalRep a = ConformalRep
  { delta :: a
  , spin  :: HalfInteger
  } deriving (Eq, Ord, Show, Generic, Binary, FromJSON, ToJSON, NFData)

instance ToTeX d => ToTeX (ConformalRep d) where
  toTeX (ConformalRep d j) = "("<>toTeX d<>")_{"<>toTeX j<>"}"

newtype Block3d = Block3d
  { unBlock3d :: Block (SO3Struct Rational Rational Delta) Q4Struct
  } deriving (Eq, Ord, Show, Generic)
    deriving newtype (ToTeX)
    deriving anyclass (Binary, NFData)

newtype IdentityBlock3d = IdentityBlock3d
  { unIdentityBlock3d :: Block (ConformalRep Rational) Q4Struct
  } deriving (Eq, Ord, Show, Generic)
    deriving anyclass (Binary, NFData)

data Block3dParams = Block3dParams
  { nmax          :: Int
  , order         :: Int
  , keptPoleOrder :: Int
  , precision     :: Int
  } deriving (Eq, Ord, Show, Generic, Binary, ToJSON, FromJSON, NFData)

type instance BlockFetchContext Block3d a m = Fetches BlockTableKey (BlockTable a) m
type instance BlockTableParams Block3d = Block3dParams

instance (RealFloat a, NFData a, KnownCoordinate c) => IsolatedBlock Block3d (Derivative c) a where
  getBlockIsolated = getBlock3dIsolated

type instance BlockFetchContext IdentityBlock3d a m = ()
type instance BlockTableParams IdentityBlock3d = Block3dParams

instance (RealFloat a, NFData a, KnownCoordinate c) => IsolatedBlock IdentityBlock3d (Derivative c) a where
  getBlockIsolated
    :: forall p m v. (HasBlocks IdentityBlock3d p m a, Applicative v, Foldable v)
    => v (Derivative c)
    -> IdentityBlock3d
    -> Compose (Tagged p) m (v a)
  getBlockIsolated derivs block@(IdentityBlock3d (Block {fourPtFunctional = Q4Struct{ q4Sign }})) =
    pure $ fmap (getDeriv q4Sign derivativeMap) derivs
    where
      derivativeMap = fromZZb q4Sign nmax' (getIdentityBlockZZB nmax' block)
      nmax' = nmax $ reflect @p Proxy
      -- This hack, suggested here
      -- (https://ghc.readthedocs.io/en/8.0.1/using-warnings.html#ghc-flag--Wredundant-constraints)
      -- suppresses the redundant constraint warning for Foldable
      _ = Foldable.length derivs

identityCrossingMat
  :: (Bounds.HasRep o (ConformalRep Rational), Eq o, Fractional a, Eq a, KnownNat n)
  => Bounds.CrossingEquations n derivs o (ConformalRep Rational) Q4Struct a
  -> CrossingMat 1 n IdentityBlock3d a
identityCrossingMat = Bounds.mapBlocks IdentityBlock3d . Bounds.identityCrossingMat

type instance BlockBase Block3d a = FourRhoCrossing a

instance (RealFloat a, NFData a, KnownCoordinate c) => ContinuumBlock Block3d (Derivative c) a where
  getBlockContinuum = getBlock3dContinuum

-- | Note that the key is independent of the internal Delta, which is
-- why this function is polymorphic. This is needed in other libraries
-- like CompositeBlock.
blockToBlockTableKey'
  :: Show d
  => Coordinate
  -> Block3dParams
  -> Block (SO3Struct Rational Rational d) Q4Struct
  -> BlockTableKey
blockToBlockTableKey' coordinate params block = case (params, block) of
  ( Block3dParams { nmax, order, keptPoleOrder, precision}
    , Block
      { struct12         = SO3Struct (ConformalRep delta1 j1) (ConformalRep delta2 j2) (ConformalRep _ j) j12 _
      , struct43         = SO3Struct (ConformalRep delta4 j4) (ConformalRep delta3 j3) (ConformalRep _ j') j43 _
      , fourPtFunctional = Q4Struct qs s
      }
    ) -> if j == j' then
    Key.BlockTableKey
    { Key.jExternal     = (j1, j2, j3, j4)
    , Key.jInternal     = j
    , Key.j12           = j12
    , Key.j43           = j43
    , Key.delta12       = mkRendered (showRationalWithPrecision precision) (delta1 - delta2)
    , Key.delta43       = mkRendered (showRationalWithPrecision precision) (delta4 - delta3)
    , Key.delta1Plus2   = mkRendered (showRationalWithPrecision precision) (delta1 + delta2)
    , Key.fourPtStruct  = qs
    , Key.fourPtSign    = s
    , Key.order         = order
    , Key.lambda        = 2*nmax - 1
    , Key.coordinates   = Set.singleton coordinate
    , Key.keptPoleOrder = keptPoleOrder
    , Key.precision     = precision
    }
    else error $ "Mismatched internal operators in block " ++ show block

blockToBlockTableKey :: Coordinate -> Block3dParams -> Block3d -> BlockTableKey
blockToBlockTableKey coord params (Block3d b) = blockToBlockTableKey' coord params b

getBlockTable
  :: forall p m  a . (HasBlocks Block3d p m a)
  => Coordinate
  -> Block3d
  -> (Compose (Tagged p) m (BlockTable a))
getBlockTable coordinate =  Compose . pure . fetch .
  blockToBlockTableKey coordinate (reflect @p Proxy)

unitarityBound :: HalfInteger -> Rational
unitarityBound j = if
  | j == 0    -> 1/2
  | j == 1/2  -> 1
  | otherwise -> fromHalfInteger j + 1

fixedDelta :: ConformalRep Delta -> Rational
fixedDelta rep = case delta rep of
  Fixed d             -> d
  RelativeUnitarity x -> unitarityBound (spin rep) + x

threePtParity :: SO3Struct a1 a2 a3 -> Parity
threePtParity (SO3Struct (ConformalRep _ j1) (ConformalRep _ j2) (ConformalRep _ j) _ j120) =
  case HalfInteger.toInteger (j1 - j2 + j - j120) of
    Just i -> parityFromIntegral i
    Nothing -> error $
      "j1 - j2 + j - j120 should be an integer, but found: " ++
      show (j1 - j2 + j - j120)

-- | Get the coordinate for a vector of derivatives. If only
-- derivatives of the form (m,0) are included, use radial coordinates
-- if they are available (XT_Radial, WS_Radial).
derivVecCoordinate :: forall v c. (Foldable v, KnownCoordinate c) => v (Derivative c) -> Coordinate
derivVecCoordinate derivVec =
  case (reflectCoordinate @c Proxy, radialDerivs) of
    (XT, True)  -> XT_Radial
    (WS, True)  -> WS_Radial
    (coord', _) -> coord'
  where
    radialDerivs = all (\(Derivative (_, n)) -> n == 0) derivVec

type BlockDR a = DR.DampedRational (FourRhoCrossing a) DerivMap a

-- | Look up a vector of derivatives in the given DerivMap obtain a
-- 'DampedRational base v a'. Throws an error if a given derivative
-- isn't present.
blockDRToBlockVector
  :: Functor v
  => v (Derivative c)
  -> DR.DampedRational base DerivMap a
  -> DR.DampedRational base v a
blockDRToBlockVector derivVec drDerivMap =
  DR.mapNumerator (\m -> fmap (\(Derivative d) -> getDeriv' m d) derivVec) drDerivMap
  where
    getDeriv' m d = case m Map.!? d of
      Just v  -> v
      Nothing -> error $
        "drDerivMap does not contain key " ++ show d ++
        ". Existing keys: " ++ show (Map.keys m)

-- | Return the internal representation of a block (along the way
-- checking that the two three-point structures have the same
-- internalRep and throwing an error if not).
internalRep :: (Show a, Show b, Eq c, Show c, Show f) => Block (SO3Struct a b c) f -> ConformalRep c
internalRep b =
  if operator3 (struct12 b) == operator3 (struct43 b)
  then
    operator3 (struct12 b)
  else
    error $ "Mismatched internal operators in block " ++ show b

-- | Turn a BlockTable into a BlockDR by looking up the appropriate
-- elements of derivMatrices. Along the way, we account for the
-- difference in convention discused in [Note: Conventions for
-- conformal blocks].
--
-- Note that this function is polymorphic in the type of the dimension
-- of the internal operator (which is why it isn't specialized to
-- Block3d). This polymorphism is needed by other libraries
-- (e.g. PolCoeffBlock in stress-tensors-3d).
blockTableToBlockDR
  :: forall a d . (Eq a, Eq d, Show d, Fractional a)
  => Coordinate
  -> Block (SO3Struct Rational Rational d) Q4Struct
  -> BlockTable a
  -> BlockDR a
blockTableToBlockDR coord b bt = drDerivMap
  where
    SO3Struct _ _ _ _ j120 = struct12 b
    SO3Struct _ _ _ _ j430 = struct43 b

    p12 = threePtParity (struct12 b)
    p43 = threePtParity (struct43 b)

    DerivMatrix j120s j430s derivs =
      let key = DerivMatrixKey p12 p43 coord
      in
        case derivMatrices bt Map.!? key of
          Just v -> v
          Nothing -> error $
            "derivMatrices does not contain key " ++ show key
            ++ ". available keys: " ++ show (Map.keys (derivMatrices bt))
            ++ ". for block: " ++ show b

    -- Implement the sign flip discussed in [Note: Conventions for
    -- conformal blocks] above.
    needSign = not (HalfInteger.isInteger (spin (internalRep b)))
               || all (not . HalfInteger.isInteger)
               [ spin . operator1 . struct12 $ b
               , spin . operator2 . struct12 $ b
               , spin . operator1 . struct43 $ b
               , spin . operator2 . struct43 $ b
               ]
    blockSignConventionFix =
      if needSign
      then DR.mapNumerator (fmap negate)
      else id

    drDerivMap =
      blockSignConventionFix $
      case (V.elemIndex j120 j120s, V.elemIndex j430 j430s) of
        (Just i, Just j) ->
          DR.mapNumerator ((Matrix.! (i+1,j+1)) . getCompose) derivs
        _ ->
          error $
          "Couldn't find (j120, j430) = " ++ show (j120, j430)
          ++ " among elements " ++ show (j120s, j430s)
          ++ " for block: " ++ show b

-- | Get a BlockVector corresponding to the given set of
-- derivatives. Also return the shift in x corresponding to the given
-- value of Delta.
getShiftAndBlockVector
  :: forall p m a c v. (HasBlocks Block3d p m a, Functor v, Foldable v, Eq a, Fractional a, KnownCoordinate c)
  => v (Derivative c)
  -> Block3d
  -> Compose (Tagged p) m (Rational, DR.DampedRational (FourRhoCrossing a) v a)
getShiftAndBlockVector derivVec b = do
  let coord = derivVecCoordinate derivVec
  bt <- getBlockTable coord b
  pure $
    let
      xShift = fixedDelta (internalRep (unBlock3d b)) - deltaMinusX bt
      drDerivMap = blockTableToBlockDR coord (unBlock3d b) bt
    in
      (xShift, blockDRToBlockVector derivVec drDerivMap)

getBlock3dContinuum
  :: (HasBlocks Block3d p m a, Functor v, Foldable v, Floating a, Eq a, KnownCoordinate c)
  => v (Derivative c)
  -> Block3d
  -> Compose (Tagged p) m (DR.DampedRational (FourRhoCrossing a) v a)
getBlock3dContinuum derivVec b = uncurry DR.shift <$> getShiftAndBlockVector derivVec b

getBlock3dIsolated
  :: (HasBlocks Block3d p m a, Functor v, Foldable v, RealFloat a, KnownCoordinate c)
  => v (Derivative c)
  -> Block3d
  -> Compose (Tagged p) m (v a)
getBlock3dIsolated derivVec b = eval <$> getShiftAndBlockVector derivVec b
  where
    eval (x, v) = DR.evalCancelPoleZeros (fromRational x) v

getIdentityBlockZZB
  :: forall a. Floating a => Int -> IdentityBlock3d -> Map.Map (Derivative 'ZZb) a
getIdentityBlockZZB nmax (IdentityBlock3d b) =
  Map.fromSet compute (Set.fromList (toList (zzbDerivsAll nmax)))
  where
    ConformalRep delta1 j1 = struct12 b
    ConformalRep _      j3 = struct43 b
    Q4Struct (q1,q2,q3,q4) s = fourPtFunctional b
    compute (Derivative (m,n)) = (compute' (m,n) + Sign.toNum s * compute' (n,m))/2
    compute' :: (Int, Int) -> a
    compute' (m,n) =
      if q1==q2 && q3==q4 then
        normalization * (2::a)**(fromRational $ (fromIntegral m)+(fromIntegral n)+2*delta1)
          * (-1)^(m+n) * fromRational (pochhammer (delta1-fromHalfInteger q1) m
            * pochhammer (delta1+fromHalfInteger q1) n)
      else 0
    normalization = (-1)^(j1mq1+j3mq3) *
      case (HalfInteger.toInteger j1, HalfInteger.toInteger j3) of
        (Just j1', Just j3') -> (-1)^(j1'+j3')
        (Just j1', Nothing)  -> (-1)^(j1' + (twice j3 - 1) `div` 2)
        (Nothing, Just j3')  -> (-1)^(j3' + (twice j1 - 1) `div` 2)
        (Nothing, Nothing)   -> (-1)^((twice j1 + twice j3) `div` 2)
      * fromInteger ((choose (twice j1) j1mq1) * (choose (twice j3) j3mq3))
    j1mq1 = fromMaybe (error err) $ HalfInteger.toInteger $ j1-q1
    j3mq3 = fromMaybe (error err) $ HalfInteger.toInteger $ j3-q3
    err = "The difference j-q must be an integer in getIdentityBlockZZB"
