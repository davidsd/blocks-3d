{-# LANGUAGE OverloadedRecordDot #-}
{-# LANGUAGE OverloadedStrings   #-}

-- | TODO: This module implements an ad-hoc logging framework that is
-- completely general and doesn't depend on the rest of blocks-3d. We
-- may want to switch to a propper logging framework at some point, or
-- move this elsewhere to a more general library.
--
-- This module is necessary because we need the flexibility to
-- redirect blocks_3d output (and associated log messages) elsewhere.

module Blocks.Blocks3d.LogConfig
  ( LogConfig(..)
  , logInfo
  , defaultLogConfig
  , withFileLogConfig
  , callProcessLogConfig
  ) where

import Data.Text      (Text)
import Data.Text.IO   qualified as Text
import Hyperion.Log   qualified as Log
import System.Exit    (ExitCode (..))
import System.IO      (IOMode (..), hFlush, withFile)
import System.Process (CreateProcess (..), StdStream (..), proc, waitForProcess,
                       createProcess_)

-- | Configuration needed for logging output when writing blocks.
data LogConfig = MkLogConfig
  { logText   :: Text -> IO ()
  , streamOut :: StdStream
  , streamErr :: StdStream
  }

defaultLogConfig :: LogConfig
defaultLogConfig = MkLogConfig Log.text Inherit Inherit

logInfo :: Show a => LogConfig -> Text -> a -> IO ()
logInfo config msg x = logText config $ Log.prettyShowInfo msg x

-- | Create a LogConfig that redirects everything to logFile, and call
-- 'go' on it.
withFileLogConfig :: FilePath -> (LogConfig -> IO r) -> IO r
withFileLogConfig logFile go =
  withFile logFile AppendMode $ \h ->
  let
    text msg = do
      now <- Log.getTimestamp
      Text.hPutStrLn h (now <> " " <> msg)
      hFlush h
  in
    go $ MkLogConfig text (UseHandle h) (UseHandle h)

-- | The same as callProcess, but using a custom LogConfig
-- (callProcess uses Inherit for both streams.)
callProcessLogConfig :: LogConfig -> FilePath -> [String] -> IO ()
callProcessLogConfig logConfig cmd args = do
  let cp = (proc cmd args)
           { delegate_ctlc = True
           , std_out = logConfig.streamOut
           , std_err = logConfig.streamErr
           }
  -- Here, it is important to use createProcess_, which does *not*
  -- close the handles passed to it (unlike createProcess,
  -- withCreateProcess, etc.).
  (_, _, _, p) <- createProcess_ "callProcessLogConfig" cp
  exit_code <- waitForProcess p
  case exit_code of
    ExitSuccess   -> return ()
    ExitFailure r -> error $
      "callProcessWithHandles:" <> cmd <> " " <> show  args <> " " <> show r
