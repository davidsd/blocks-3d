{-# LANGUAGE MultiWayIf          #-}
{-# LANGUAGE OverloadedRecordDot #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE StaticPointers      #-}
{-# LANGUAGE TypeFamilies        #-}
{-# OPTIONS_GHC -fno-warn-orphans #-} -- for SDPFetchValue


module Blocks.Blocks3d.Build where

import Blocks                        (Coordinate (..))
import Blocks.Blocks3d               qualified as B3d
import Blocks.Blocks3d.BlockTableKey (BlockTableKey' (..))
import Blocks.Sign                   qualified as Sign
import Bootstrap.Build               (BuildLink (..))
import Bootstrap.Math.HalfInteger    (HalfInteger, isInteger)
import Control.Distributed.Process   (Process)
import Control.Monad.IO.Class        (liftIO)
import Control.Monad.Reader          (asks, local)
import Data.List                     qualified as List
import Data.Map.Strict               (Map)
import Data.Map.Strict               qualified as Map
import Data.PQueue.Prio.Max          qualified as MaxQueue
import Data.Set                      (Set)
import Data.Void                     (Void)
import Hyperion                      (Job, cAp, cPure, doConcurrently_,
                                      jobNodeCpus, remoteEval,
                                      serviceIdToString, setTaskCpus,
                                      withServiceId)
import Hyperion.Bootstrap.Bound      (BoundConfig (..), BoundFiles,
                                      SDPFetchValue, blockDir)
import Hyperion.Log                  qualified as Log
import Hyperion.Slurm                qualified as Slurm
import Hyperion.Util                 (minute)
import Hyperion.Util.MapMonitored    (mapConcurrentlyMonitored)
import Hyperion.WorkerCpuPool        (NumCPUs (..))
import System.Directory              (createDirectoryIfMissing, doesFileExist)
import System.FilePath.Posix         (dropExtension, (<.>), (</>))

type instance SDPFetchValue a B3d.BlockTableKey = B3d.BlockTable a

----------------- Memory model ------------------

-- Here we develop a crude model of the memory usage of blocks_3d, as
-- of commit ecddcc374eaf07435b4ebf0b8d9db8c822fb88f5. The two largest
-- data structures are the residues array, and the array of
-- blocks. Our model gives a reasonable estimate of the size of these
-- data structures in bytes, within 15%. However, the actual memory
-- usage is larger. We apply 'fudgeFunction' to get our final
-- estimate, which we have checked in Mathematica is a conservative
-- overestimate in several examples.

type Bytes = Int

-- | Sum over parity of the number of elts of each residue matrix
residueMatrixNumElts :: HalfInteger -> HalfInteger -> Int
residueMatrixNumElts j12 j43 = 
  (1 + floor j12) * (1+floor j43) + floor (j12 + 1/2) * floor (j43 + 1/2)

-- | The number of bytes associated to a residue matrix, summed over
-- pseudoparity. 'eltBytes' is the number of bytes for a single
-- element.
residueMatrixBytesParitySum :: HalfInteger -> HalfInteger -> Bytes -> Bytes
residueMatrixBytesParitySum j12 j43 eltBytes =
  residueMatrixNumElts j12 j43 * eltBytes + (numMatrices + numMatrixRows)*stdVectorBytes
  where
    -- sizeof(v), where v has type std::vector<...>
    stdVectorBytes = 24

    -- Sum over parity of the number of rows of each matrix
    numMatrixRows = (1 + floor j12) + floor (j12 + 1/2)

    -- One matrix for each parity
    numMatrices = 2

-- | The number of matrices in the residue array, for a fixed
-- pseudoparity.
residueNumMatrices :: Int -> Int -> Int
residueNumMatrices jMax order = order*order*(5*jMax + order) `div` 4

-- | The approximate number of bytes in the residue array
residueArrayBytes :: BlockTableKey' (Set HalfInteger) -> Bytes
residueArrayBytes key =
  residueNumMatrices jMax (fromIntegral key.order) *
  residueMatrixBytesParitySum key.j12 key.j43 floatBytes
  where
    floatBytes = ceiling (toRational key.precision / 8)
    jMax = ceiling (maximum key.jInternal)

-- | The approximate number of bytes in the block array.
--
-- NB: For radial blocks, we should be able to set numDerivs =
-- lambda. However, such a model does not seem to work well for radial
-- blocks -- it substantially underestimates the memory usage. So for
-- now, we do not distinguish between radial and non-radial
-- blocks. The radial blocks will take somewhat less memory, but this
-- is OK, since they will still lie below our conservative estimate.
blockArrayBytes :: BlockTableKey' (Set HalfInteger) -> Bytes
blockArrayBytes key =
  numJs * numDerivs * residueMatrixBytesParitySum key.j12 key.j43 eltBytes
  where
    stdVectorBytes = 24
    floatBytes     = ceiling (toRational key.precision / 8)
    numJs          = fromIntegral $ length key.jInternal
    nmax           = floor $ (toRational key.lambda + 1)/2
    numDerivs      = nmax * (nmax + 1)
    numPoles       = fromIntegral key.keptPoleOrder * 5 `div` 2
    polDegree      = numPoles + fromIntegral key.lambda
    eltBytes       = floatBytes * polDegree + stdVectorBytes

-- | Approximate memory usage of blocks3d in bytes. This should be a
-- conservative overestimate. The fudgeFunction is chosen so that data
-- points from testing lie below this estimate.
blockMemoryApprox :: BlockTableKey' (Set HalfInteger) -> Rational
blockMemoryApprox key =
  fudgeFunction . toRational $ residueArrayBytes key + blockArrayBytes key
  where
    fudgeFunction x = min (5e8 + 2.3*x) (1.2e9 + 1.6*x)

-- | Check if a coordinate is radial
isRadialCoordinate :: Coordinate -> Bool
isRadialCoordinate c = c `elem` [XT_Radial, WS_Radial]

-- | Check if a BlockTableKey has only radial coordinates
isRadialKey :: BlockTableKey' j -> Bool
isRadialKey key = all isRadialCoordinate key.coordinates

-- | An approximation for the "cost" of a block, where cost is
-- proportional to the single-threaded elapsed time. This crude
-- approximation seems to be consistent with experimental data.
blockCostApprox :: BlockTableKey' (Set HalfInteger) -> Rational
blockCostApprox key =
  toRational (residueArrayBytes key + blockArrayBytes key) / radialCorrection
  where
    radialCorrection
      | isRadialKey key =
        toRational $
        floor @_ @Int $
        (2 + toRational key.lambda + Sign.toNum key.fourPtSign / 2) / 2
      | otherwise = 1

data PoleType = I | II | III | IV
  deriving (Enum, Bounded)

-- | This is a copy of the pole_range_k function in
-- blocks_3d/src/lambda_log_r_tables/Pole_Types.cxx
poleRangeK :: PoleType -> HalfInteger -> HalfInteger -> HalfInteger -> Int -> Int
poleRangeK poleType j j12 j43 order = case poleType of
  I  -> order
  II -> min (floor j) order
  III
    | isInteger j -> order `div` 2
    | otherwise   -> minimum
      [floor j, floor j12, floor j43, order `div` 2]
  IV
    | isInteger j -> minimum
      [floor j, floor j12, floor j43, (order + 1) `div` 2]
    | otherwise   -> (order + 1) `div` 2

blockNumPoles :: BlockTableKey' HalfInteger -> Int
blockNumPoles key = sum $ do
  poleType <- [minBound .. maxBound]
  pure $ poleRangeK poleType key.jInternal key.j12 key.j43 key.keptPoleOrder

blockFileSize :: BlockTableKey' HalfInteger -> Bytes
blockFileSize key =
  numDerivs * residueMatrixNumElts key.j12 key.j43 * polDegree * floatBytes
  where
    nmax = floor $ (toRational key.lambda + 1)/2
    numDerivs =
      if isRadialKey key
      then fromIntegral key.lambda
      else nmax * (nmax + 1)
    polDegree = fromIntegral (blockNumPoles key + key.lambda)
    -- We add 10 to account for additional characters accompanying
    -- each float, such as commas, quotation marks, etc. This comes
    -- from Vasiliy's experiments for precision 1024 which show that
    -- precision*log 2/log 10 is about 308, while the correct number
    -- of bytes per float is about 318.
    floatBytes = round (realToFrac @_ @Double key.precision * log 2 / log 10) + 10

----------------- Build routines ------------------

-- | Insert a key value pair, treating Map k [v] as a multimap from
-- k's to v's.
multiMapInsert :: Ord k => k -> v -> Map k [v] -> Map k [v]
multiMapInsert x y = Map.alter (add y) x
  where
    add z Nothing   = Just [z]
    add z (Just zs) = Just (z:zs)

-- | Given a list [(m1,n1),(m2,n2),...] of nodes ni with memory mi,
-- and a list [(r1,b1),(r2,b2),...] of blocks bi with memory
-- requirements ri, greedily distribute blocks between nodes, starting
-- with the largest blocks. If all blocks can be distributed between
-- nodes without exceeding the nodes' memory, then return the
-- distribution of blocks. If not, return Nothing.
divideAmongNodes
  :: (Num r, Ord r, Ord n, Ord a)
  => [(r,n)]
  -> [(r,a)]
  -> Maybe (Map n [(r,a)])
divideAmongNodes nodes blocks =
  go (MaxQueue.fromList nodes) Map.empty (reverse $ List.sort blocks)
  where
    go _        nodeMap [] = Just nodeMap
    go memQueue nodeMap (block@(blockMem,_) : bs) =
      let
        ((nodeMem, node), memQueue') = MaxQueue.deleteFindMax memQueue
      in
        if blockMem <= nodeMem
        then
          go
          (MaxQueue.insert (nodeMem - blockMem) node memQueue')
          (multiMapInsert node block nodeMap)
          bs
        else
          Nothing

-- | Build a set of Block3d's. First, we determine the approximate
-- amount of memory needed for each block computation. By default, we
-- assign a number of threads to each block proportional to the
-- fraction of a node's memory. If the total set of blocks is small
-- enough that we can simultaneously fit them on all nodes, then we
-- split them into Node Groups and evaluate them at once. Otherwise,
-- we compute in a round-robin fashion, allotting each block its
-- default number of threads, starting from the largest blocks.
buildAllBlocks3d
  :: BoundConfig
  -> BoundFiles
  -> Set (BlockTableKey' HalfInteger)
  -> Job ()
buildAllBlocks3d config files keysUngrouped = do
  nodes <- liftIO Slurm.getJobNodes
  NumCPUs nodeCpus <- asks jobNodeCpus
  let
    keys = B3d.groupBlockTableKeys keysUngrouped
    -- Underestimate the memory for safety
    nodeMem = 240 * 1000 * 1000 * 1000
    -- The default number of threads is proportional to the fraction
    -- of the node's memory needed by the key. NB: By using 'ceiling',
    -- we ensure that the number of threads is always >= 1. 'min
    -- nodeCpus' ensures that we never try to use more than the
    -- maximal number of threads, even if we expect to take a lot of
    -- memory. In the case where 'keyThreads k == nodeCpus', we might
    -- run out of memory, but we might as well try anyway.
    keyThreads k =
      min nodeCpus $
      ceiling $
      (blockMemoryApprox k / nodeMem) * toRational nodeCpus
    keysWithThreads = [(keyThreads k,k) | k <- keys]
    blocks3dExecutable = scriptsDir config </> "blocks_3d.sh"
    debugLevel = B3d.Debug
  case divideAmongNodes [(nodeCpus,n) | n <- nodes] keysWithThreads of
    Just nodeMap -> do
      -- Map.elems forgets the specific node for each Node Job. Since
      -- all the nodes are equivalent, it doesn't really matter which
      -- Node Job gets run where.
      let
        nodeGroups = Map.elems nodeMap
        nodeGroupKeys = map (map snd) nodeGroups
        nodeGroupThreads = map (map fst) nodeGroups
      Log.info "Computing Block3d's" (length keys)
      Log.info "Grouped Block3d's into Node Groups; nodeGroupThreads" nodeGroupThreads
      mapConcurrentlyMonitored "Block3d Node Groups" (2*minute)
        (remoteRunBlocks3dNodeGroup blocks3dExecutable files debugLevel nodeCpus)
        nodeGroupKeys
    Nothing ->
      mapConcurrentlyMonitored "Block3d's" (2*minute)
      (remoteRunBlocks3d blocks3dExecutable files debugLevel) $
      reverse (List.sort keysWithThreads)

remoteRunBlocks3d
  :: FilePath
  -> BoundFiles
  -> B3d.DebugLevel
  -> (Int, BlockTableKey' (Set HalfInteger))
  -> Job ()
remoteRunBlocks3d blocks3dExecutable files debugLevel (numThreads, key) =
  local (setTaskCpus (NumCPUs numThreads)) $ do
  remoteEval $ cAp (static liftIO) $
    static B3d.writeBlockTables `cAp`
    cPure blocks3dExecutable `cAp`
    cPure numThreads `cAp`
    cPure debugLevel `cAp`
    cPure (blockDir files) `cAp`
    cPure key

remoteRunBlocks3dNodeGroup
  :: FilePath
  -> BoundFiles
  -> B3d.DebugLevel
  -> Int
  -> [BlockTableKey' (Set HalfInteger)]
  -> Job ()
remoteRunBlocks3dNodeGroup blocks3dExecutable files debugLevel nodeCpus keys =
  local (setTaskCpus (NumCPUs nodeCpus)) $ do
  remoteEval $
    static runBlocks3dNodeGroup `cAp`
    cPure blocks3dExecutable `cAp`
    cPure files `cAp`
    cPure debugLevel `cAp`
    cPure nodeCpus `cAp`
    cPure keys

-- | A list of integers of length 'len' whose sum is 'excess', which
-- is as close to constant as possible, and nonincreasing.
adjustments :: Int -> Int -> [Int]
adjustments len excess =
  let (q,r) = excess `quotRem` len
  in replicate r (q+1) ++ replicate (len-r) q

-- | Given 'targetSum' and a list of positive numbers 'ns', rescale
-- the numbers, rounding to integers so that their new sum equals
-- targetSum. We preferentially round up earlier numbers and round
-- down later numbers. All resulting integers should be nonzero
-- Satisfies:
--
-- > sum (resizeList targetSum l) == targetSum
-- > all (/= 0) (resizeList targetSum l)
--
-- For example:
--
-- > resizeList 100 [1,2,3] == [17,33,50]
-- > resizeList 100 [0.0001,0.0001,1] == [1,1,98]
resizeList :: Int -> [Rational] -> [Int]
resizeList targetSum ns = go targetSum
  where
    len = length ns
    total = sum ns

    go :: Int -> [Int]
    go target =
      let
        -- Rescaled to sum to target, with floor applied. Should
        -- satisfy: target - len <= sum resized <= target
        resized = [ floor ((n / total) * toRational target) | n <- ns ]
        -- Satisfies (targetSum - target) <= excess <= len +
        -- (targetSum - target). In particular, excess is positive.
        excess = targetSum - sum resized
        numZeros = length (takeWhile (== 0) resized)
      in
        if excess >= numZeros
        then
          -- we can remove all zeros using adjustments
          zipWith (+) resized (adjustments len excess)
        else
          -- zeros will remain. Reduce the target and try again.
          go (target-1)

-- | Compute a group of blocks assigned to a single node. We allot
-- each block a number of threads proportional to its memory usage,
-- such that the total number of threads adds up to nodeCpus.
--
-- Since we know that memory will not be a problem, we can
-- allocate according to performance. For this, it is important that
-- radial blocks are much faster. They should get fewer threads,
-- despite taking a similar amount of memory.
runBlocks3dNodeGroup
  :: FilePath
  -> BoundFiles
  -> B3d.DebugLevel
  -> Int
  -> [BlockTableKey' (Set HalfInteger)]
  -> Process ()
runBlocks3dNodeGroup blocks3dExecutable files debugLevel nodeCpus keysUnsorted = do
  let
    -- Sort by costs in _increasing_ order, so that resizeList
    -- preferentially rounds up smaller thread numbers
    (costs, keys) = unzip $ List.sort [(blockCostApprox k, k) | k <- keysUnsorted]
    -- Here it is important that length keys <= nodeCpus, so that each
    -- process can get at least one thread. This is guaranteed by the
    -- construction of the Node Groups.
    resizedThreads = resizeList nodeCpus costs
  Log.info "Block3d Node Group numKeys" (length keys)
  Log.info "Block3d Node Group (cost, numThreads)" $
    zip (map (fromRational @Double) costs) resizedThreads
  doConcurrently_ $ do
    (numThreads, key) <- zip resizedThreads keys
    pure $
      withServiceId $ \serviceId -> do
      logFile_maybe <- Log.getLogFile
      case logFile_maybe of
        Nothing -> error "Couldn't get current log file"
        Just logFile -> liftIO $ do
          let
            blockLogDir = dropExtension logFile
            blockLogFile = blockLogDir </> serviceIdToString serviceId <.> "log"
          createDirectoryIfMissing True blockLogDir
          B3d.withFileLogConfig blockLogFile $ \logConfig -> do
            Log.info "Computing Block3d with numThreads" (numThreads, serviceId)
            B3d.writeBlockTablesCustomLog logConfig
              blocks3dExecutable numThreads debugLevel (blockDir files) key
            Log.info "Finished Block3d with numThreads" (numThreads, serviceId)

block3dBuildLink
  :: BoundConfig
  -> BoundFiles
  -> BuildLink Job B3d.BlockTableKey Void
block3dBuildLink config files = BuildLink
  { buildDeps = const []
  , checkCreated = liftIO . doesFileExist . B3d.blockTableFilePath (blockDir files)
  , buildAll = buildAllBlocks3d config files
  }

