{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE OverloadedStrings #-}

module Main where

import Blocks.Blocks3d.ReadTable            (readBlockFile)
import Control.DeepSeq                      (deepseq)
import Control.Monad.Trans.Resource         (runResourceT)
import Data.Attoparsec.ByteString.Char8     qualified as Atto
import Data.Attoparsec.ByteString.Streaming qualified as StreamingAtto
import Data.ByteString.Char8                qualified as ByteString
import Data.Monoid                          (Sum (..))
import Data.Proxy                           (Proxy)
import Data.Reflection                      (reifyNat)
import Numeric.Rounded                      (Rounded, RoundingMode (..))
import Streaming                            qualified as Streaming
import Streaming.ByteString                 (ByteStream)
import Streaming.ByteString                 qualified as ByteStream
import Streaming.Prelude                    qualified as Streaming
import System.Environment                   (getArgs)

type BigFloat p = Rounded 'TowardZero p

-- | Below is a simple test of streaming parsing, where we read a file
-- with many strings representing floating point numbers, and process
-- it in different ways using streaming parsing. Most relevant for us
-- is 'listNumberFile', which parses to a list of BigFloat's, since
-- that is what we need to do when reading blocks.

myNumberFilePath :: FilePath
myNumberFilePath = "/home/dsd/projects/blocks-3d/numbers.txt"

-- | Write 100000 copies of pi with precision 1024 to a file. The file
-- is about 30MB.
writeNumberFile :: IO ()
writeNumberFile = ByteString.writeFile myNumberFilePath numberString
  where
    numberString =
      mconcat $
      replicate 100000 $
      ByteString.pack $
      show @(BigFloat 1024) pi <> "\n"

parseNumberStream
  :: (Monad m, Fractional a)
  => ByteStream m r
  -> Streaming.Stream (Streaming.Of a) m (Either (StreamingAtto.Errors, ByteStream m r) r)
parseNumberStream = StreamingAtto.parsed (Atto.rational <* Atto.skipSpace)

-- | This nicely runs in constant space, taking only about 150K of
-- memory.
sumNumberFile :: Fractional a => FilePath -> IO (Sum a)
sumNumberFile file = runResourceT $ do
  result Streaming.:> _ <- Streaming.foldMap Sum $
    parseNumberStream $
    ByteStream.readFile file
  pure result

listNumberFile :: Fractional a => FilePath -> IO [a]
listNumberFile file = runResourceT $ do
  result Streaming.:> _ <- Streaming.toList $
    parseNumberStream $
    ByteStream.readFile file
  pure result

-- | Read the numbers from the file using streaming parsing, and
-- collect them into a list. We observe that the memory usage goes
-- down with the precision of the BigFloat, in accordance with the
-- fact that we should not be building up ByteString's from the file
-- in memory.
--
-- Max residency:
-- - precision 1024 -> 20MB
-- - precision 832  -> 18MB
-- - precision 640  -> 16MB
-- - precision 448  -> 13.8MB
-- - precision 256  -> 11MB
--
-- NB: One tricky thing about heap profiling in this case is that both
-- Rounded's (i.e. BigFloat's) and ByteString's store their data in
-- ByteArray# (which shows up as ARR_WORDS in a heap profile).
testReadNumberFile :: IO ()
testReadNumberFile = do
  precString : _ <- getArgs
  let precision :: Integer = read precString
  reifyNat precision $ \(_ :: Proxy p) -> do
    res <- listNumberFile @(BigFloat p) myNumberFilePath
    res `deepseq`
      putStrLn "Done."

myBlockFile :: FilePath
myBlockFile = "/expanse/lustre/scratch/dsd/temp_project/data/2024-08/PTrAJ/Object_5elH5oNA3rS_ooF42-PpxQo938uwzVNdoCekzk6Z6s4/blocks/blocks_3d_tables_rEqxp2rbgmkqvFrUE-bV9qm_XhkYK2OVSEvnydRKk2k/spin_2.0.json"

testReadBlockTable :: IO ()
testReadBlockTable = do
  block <- readBlockFile @(BigFloat 832) myBlockFile
  block `deepseq`
    putStrLn "Done."

main :: IO ()
main = testReadBlockTable
